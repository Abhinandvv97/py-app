FROM ubuntu:latest

COPY pricing.py /

RUN apt update

RUN apt-get update && apt-get install -y software-properties-common gcc && \
    add-apt-repository -y ppa:deadsnakes/ppa

RUN apt-get update && apt-get install -y python3.6 python3-distutils python3-pip python3-apt
RUN pip3 install boto3
RUN pip3 install mysql-connector-python

# Installing prerequisite packages
RUN apt-get -y install curl unzip groff less
# AWS CLI installation commands
RUN     curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
RUN     unzip awscliv2.zip && ./aws/install

ENV AWS_ACCESS_KEY_ID=AKIAYV53VXXYSKYQYPWD
ENV AWS_SECRET_ACCESS_KEY=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
ENV AWS_DEFAULT_REGION=ap-south-1
ENTRYPOINT ["python3", "pricing.py"]
