from itertools import count
from multiprocessing.spawn import old_main_modules
import boto3,json
import mysql.connector as c
count = 0
con2 = c.connect(host = "localhost",user = "root", password = "Pass@123",database = 'mydb')
mycursor2 = con2.cursor()
client = boto3.client('pricing')
paginator = client.get_paginator('get_products')
iterator = paginator.paginate(ServiceCode='AmazonEC2')
for ins in iterator:
    attr = ins['PriceList']
    for res in attr:
        res = json.loads(res)
        included_keys = ['product','attributes','terms','OnDemand','priceDimensions','pricePerUnit']
        dict1 =   {k:v for k,v in res.items() if k in included_keys}
        pro = dict1['product']
        attr = pro['attributes']
        ivalue =  attr.get('instanceType')
        region = attr.get('regionCode')
        loc = attr.get('location')
        os_type = attr.get('operatingSystem')
        terms = dict1['terms']
        on_demand = terms.get('OnDemand',{})
        values = on_demand.values()
        #print(values)
        for price_dimension in values:
            pr = price_dimension.get('priceDimensions',{})
            price_unit = pr.values()
            v = list(price_unit)[0]
            pr_unit = v.get('pricePerUnit',{})
            #print(f"{ivalue} - {os_type} - {pr_unit} - {region}  -  {loc}")
            price_val = pr_unit.values()
            price_ec2 = list(price_val)[0]
            insert_data = "INSERT INTO ec2(Ec2_name,Location,Region,OS,Price)VALUES(%s,%s,%s,%s,%s)"
            val = (ivalue,loc,region,os_type,price_ec2)
            mycursor2.execute(insert_data,val)
            con2.commit()
print("Data Successfully Inserted")
